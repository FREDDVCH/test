	var operationS = '';
	var currentValue = 0;
	// var inputValue = document.getElementById('screen');
	var checkValue = true;
	// var body = document.querySelector('body')
	// var calcBody = document.querySelector("#calc-body")
	// var numPad = document.querySelector('#num-pad');
   	var toggle = true;
	var keyCode = {
		48 : "0",
		49 : "1",
		50 : "2",
		51 : "3",
		52 : "4",
		53 : "5",
		54 : "6",
		55 : "7",
		56 : "8",
		57 : "9",
		96 : "0",
		97 : "1",
		98 : "2",
		99 : "3",
		100 : "4",
		101 : "5",
		102 : "6",
		103 : "7",
		104 : "8",
		105 : "9",
		8 : 'del',
		46 : 'del',
		27 :'del',
		110 : '.',
		111 : '/',
		106 : '*',
		109 : '-',
		107 : '+',
		187 : '=',
		13 : '='
	}

    function changeSkin(){
    	if (toggle){
    		toggle = false;
    		calcBody.className = "";
    	} else {
    		toggle = true;
    		calcBody.className = "ndSkin";
    	}

    }
    function hideCalc () {

    	if (toggle) {
    		toggle = false;
			numPad.style.display = 'none';
			inputValue.style.display = 'none';
			calcBody.style.height = '0px';
    	} else {
    		toggle = true;
			numPad.style.display = '';
			inputValue.style.display = 'block';
			calcBody.style.height = '';	
    	}	
	}
    
	body.onkeydown = function (e) {
		if (keyCode[e.keyCode] < 10) {
			pressNumber(keyCode[e.keyCode]);
		} else if (keyCode[e.keyCode] == 'del') {
			allClear();
		} else if (operation(keyCode[e.keyCode]) != undefined) {
			operation(keyCode[e.keyCode])
		}
	}

	function changeFontSize() {
		var inputLength = inputValue.value.length;

		if (inputValue.value.length > 7 && inputValue.value.length < 14) {
			document.getElementById('screen').style.fontSize = 50 - inputLength * 1.2 + "px";
		} else if (inputValue.value.length > 14) {
			document.getElementById('screen').style.fontSize = 16.8 + "px";
		} else if (inputValue.value.length <= 1) {
			document.getElementById('screen').style.fontSize = 50 + "px";
		}
	}

	
	function pressNumber(number) {
		if (checkValue) {
			if (inputValue.value == "0") {
				inputValue.value = number;
			} else {
				inputValue.value += number;
			}
		} else {
			inputValue.value = number;
			checkValue = true;
		}
		changeFontSize();
	}

	function operation(sign) {

		var inputScreen = inputValue.value;

		if (!(checkValue) && operationS != "=") {
			console.log()
			inputValue.value = currentValue;
		} else {
			checkValue = false;
			if ('+' == operationS)
				currentValue += parseFloat(inputScreen);
			else if ('-' == operationS)
				currentValue -= parseFloat(inputScreen);
			else if ('/' == operationS)
				currentValue /= parseFloat(inputScreen);
			else if ('*' == operationS)
				currentValue *= parseFloat(inputScreen);
			else
				currentValue = parseFloat(inputScreen);
			inputValue.value = currentValue;
			operationS = sign;
			changeFontSize()
		}
	}

	function pressDot() {
		var currentInpVal = inputValue.value;
		if (!(checkValue)) {
			currentInpVal = "0.";
			checkValue = true;
		} else {
			if (currentInpVal.indexOf(".") == -1) {
				currentInpVal += ".";
			}
			inputValue.value = currentInpVal;
		}
	}

	function allClear() {
		currentValue = 0;
		operationS = false;
		clearInput();
		changeFontSize();
	}

	function clearInput() {
		inputValue.value = "0";
		checkValue = false;
	}

	function changeNumber() {
		var currentSignValue = inputValue.value;
		currentSignValue *= parseFloat(-1);
		inputValue.value = currentSignValue;
	}

	function percent() {
		var currentInputValue = inputValue.value;
		currentInputValue = (parseFloat(inputValue.value) / 100) * parseFloat(currentValue);
		inputValue.value = currentInputValue;
	}