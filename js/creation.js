var div = document.createElement('div');
div.setAttribute('id', 'calc-body');
function newCalc (argument) {
	createMenu();
	createInput();
	createNumPad();
	addClassForNum();

	var buttonCalc = document.querySelector('#buttonCalc');
	buttonCalc.setAttribute('disabled','disabled');

	inputValue = document.getElementById('screen');
	body = document.querySelector('body')
	calcBody = document.querySelector("#calc-body")
	numPad = document.querySelector('#num-pad');
}
function createMenu() {

	var ul = document.createElement('ul');
	setAttributes(ul, {
		'class': 'close-menu'
	});

	document.body.appendChild(div);
	div.appendChild(ul);
	// create menu
	var li = document.createElement('li');
	var li2 = document.createElement('li');
	var li3 = document.createElement('li');

	setAttributes(li, {
		'id': 'exit',
		'class': 'min-red-buttom round-buttom',
		'onClick': 'hideCalc()'
	})
	setAttributes(li2, {
		'id': 'expand',
		'class': 'min-yellow-buttom round-buttom',
		'onClick': ''
	})
	setAttributes(li3, {
		'id': 'turn',
		'class': 'min-green-buttom round-buttom',
		'onClick': 'changeSkin()'
	})

	ul.appendChild(li);
	ul.appendChild(li2);
	ul.appendChild(li3);
}
function createInput() {

	var input = document.createElement('input');
	setAttributes(input, {
		'id': 'screen',
		'class': 'digital-screen',
		'type': 'text',
		'placeholder': '0'
	});
	div.appendChild(input);
}
function createNumPad() {

	var numPad = document.createElement('ul');
	setAttributes(numPad, {
		'id': 'num-pad',
		'class': 'num-pad'
	});
	
	div.appendChild(numPad);

	var padli = document.createElement('li');
	setAttributes(padli, {'class':'num-pad-item'}); 

	var li1 = document.createElement('li');
	setAttributes(li1,{'onClick':'allClear()'});
	li1.innerHTML = '<p>AC</p>';

	var li2 = document.createElement('li');
	setAttributes(li2,{'onClick':'changeNumber()'})
	li2.innerHTML = '<p>+/-</p>';

	var li3 = document.createElement('li');
	setAttributes(li3,{'onClick':'percent()'})
	li3.innerHTML = '<p>%</p>';

	var li4 = document.createElement('li');
	setAttributes(li4,{"onClick":"operation('/')"})
	li4.innerHTML = '<p>÷</p>';

	var li5 = document.createElement('li');
	setAttributes(li5,{"onClick":"pressNumber('7')"})
	li5.innerHTML = '<p>7</p>';

	var li6 = document.createElement('li');
	setAttributes(li6,{"onClick":"pressNumber('8')"})
	li6.innerHTML = '<p>8</p>';

	var li7 = document.createElement('li');
	setAttributes(li7,{"onClick":"pressNumber('9')"})
	li7.innerHTML = '<p>9</p>';

	var li8 = document.createElement('li');
	setAttributes(li8,{"onClick":"operation('*')"})
	li8.innerHTML = '<p>×</p>';

	var li9 = document.createElement('li');
	setAttributes(li9,{"onClick":"pressNumber('4')"})
	li9.innerHTML = '<p>4</p>';

	var li10 = document.createElement('li');
	setAttributes(li10,{"onClick":"pressNumber('5')"})
	li10.innerHTML = '<p>5</p>';

	var li11 = document.createElement('li');
	setAttributes(li11,{"onClick":"pressNumber('6')"})
	li11.innerHTML = '<p>6</p>';

	var li12 = document.createElement('li');
	setAttributes(li12,{"onClick":"operation('-')"})
	li12.innerHTML = '<p>-</p>';

	var li13 = document.createElement('li');
	setAttributes(li13,{"onClick":"pressNumber('1')"})
	li13.innerHTML = '<p>1</p>';

	var li14 = document.createElement('li');
	setAttributes(li14,{"onClick":"pressNumber('2')"})
	li14.innerHTML = '<p>2</p>';

	var li15 = document.createElement('li');
	setAttributes(li15,{"onClick":"pressNumber('3')"})
	li15.innerHTML = '<p>3</p>';

	var li16 = document.createElement('li');
	setAttributes(li16,{"onClick":"operation('+')"})
	li16.innerHTML = '<p>+</p>';

	var li17 = document.createElement('li');
	setAttributes(li17,{"onClick":"pressNumber('0')", "class":"wide-button"})
	li17.innerHTML = '<p>0</p>';

	var li18 = document.createElement('li');
	setAttributes(li18,{"onClick":'pressDot()'})
	li18.innerHTML = '<p>.</p>';

	var li19 = document.createElement('li');
	setAttributes(li19,{"onClick":"operation('=')"})
	li19.innerHTML = '<p>=</p>';
	
	numPad.appendChild(li1);
	numPad.appendChild(li2);
	numPad.appendChild(li3);
	numPad.appendChild(li4);
	numPad.appendChild(li5);
	numPad.appendChild(li6);
	numPad.appendChild(li7);
	numPad.appendChild(li8);
	numPad.appendChild(li9);
	numPad.appendChild(li10);
	numPad.appendChild(li11);
	numPad.appendChild(li12);
	numPad.appendChild(li13);
	numPad.appendChild(li14);
	numPad.appendChild(li15);
	numPad.appendChild(li16);
	numPad.appendChild(li17);
	numPad.appendChild(li18);
	numPad.appendChild(li19);
}
function addClassForNum () {
	var numPad = document.querySelector('#num-pad').getElementsByTagName('li');

	var len = numPad.length
	var counter = 0

	while(counter < len){
		var li = numPad[counter];
        li.className += ' num-pad-item';
		counter++;
        // setAttributes(li,{'class':'num-pad-item');
	}
}
function setAttributes(el, attrs) {
	for (var key in attrs) {
		el.setAttribute(key, attrs[key]);
	}
}